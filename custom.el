(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
   ["#181818" "#ab4642" "#a1b56c" "#f7ca88" "#7cafc2" "#ba8baf" "#7cafc2" "#d8d8d8"])
 '(ansi-term-color-vector
   [unspecified "#181818" "#ab4642" "#a1b56c" "#f7ca88" "#7cafc2" "#ba8baf" "#7cafc2" "#d8d8d8"] t)
 '(arduino-mode-home "/home/alex/src/arduino/")
 '(auth-sources '(password-store))
 '(backup-by-copying t)
 '(backup-directory-alist '(("." . "/home/alex/.emacs.d/saves")))
 '(browse-url-browser-function 'eww-browse-url)
 '(custom-safe-themes
   '("16dd114a84d0aeccc5ad6fd64752a11ea2e841e3853234f19dc02a7b91f5d661" default))
 '(delete-old-versions t)
 '(dired-listing-switches "-alh")
 '(dired-omit-files "^\\...+$")
 '(epa-pinentry-mode 'loopback)
 '(epg-pinentry-mode 'loopback)
 '(exec-path-from-shell-variables '("PATH" "MANPATH" "GERBIL_HOME"))
 '(forge-alist
   '(("github.com-arbitrary" "api.github.com" "github.com" forge-github-repository)
     ("github.com" "api.github.com" "github.com" forge-github-repository)
     ("gitlab.com" "gitlab.com/api/v4" "gitlab.com" forge-gitlab-repository)
     ("salsa.debian.org" "salsa.debian.org/api/v4" "salsa.debian.org" forge-gitlab-repository)
     ("framagit.org" "framagit.org/api/v4" "framagit.org" forge-gitlab-repository)
     ("gitlab.gnome.org" "gitlab.gnome.org/api/v4" "gitlab.gnome.org" forge-gitlab-repository)
     ("codeberg.org" "codeberg.org/api/v1" "codeberg.org" forge-gitea-repository)
     ("code.orgmode.org" "code.orgmode.org/api/v1" "code.orgmode.org" forge-gogs-repository)
     ("bitbucket.org" "api.bitbucket.org/2.0" "bitbucket.org" forge-bitbucket-repository)
     ("git.savannah.gnu.org" nil "git.savannah.gnu.org" forge-cgit**-repository)
     ("git.kernel.org" nil "git.kernel.org" forge-cgit-repository)
     ("repo.or.cz" nil "repo.or.cz" forge-repoorcz-repository)
     ("git.suckless.org" nil "git.suckless.org" forge-stagit-repository)
     ("git.sr.ht" nil "git.sr.ht" forge-srht-repository)))
 '(geiser-active-implementations '(mit))
 '(geiser-implementations-alist '(((regexp "\\.pkg$") mit)))
 '(gnus-always-read-dribble-file t)
 '(gnus-article-save-directory "~/news/")
 '(gnus-cache-directory "~/news/cache/")
 '(gnus-check-new-newsgroups nil)
 '(gnus-directory "~/news/")
 '(gnus-expert-user t)
 '(gnus-inhibit-startup-message t)
 '(gnus-interactive-exit nil)
 '(gnus-kill-files-directory "~/news/")
 '(gnus-novice-user nil)
 '(gnus-read-active-file nil)
 '(gnus-secondary-select-methods '((nntp "")))
 '(gnus-select-method '(nntp "nntp.aioe.org"))
 '(helm-M-x-fuzzy-match t)
 '(helm-buffers-fuzzy-matching t)
 '(helm-completion-style 'emacs)
 '(helm-input-idle-delay 0.2)
 '(helm-scroll-amount 8)
 '(helm-split-window-inside-p t)
 '(hl-paren-background-colors '("#e8fce8" "#c1e7f8" "#f8e8e8"))
 '(hl-paren-colors '("#40883f" "#0287c8" "#b85c57"))
 '(hl-sexp-background-color "#1c1f26")
 '(inferior-acl2-program "/home/alex/src/acl2-8.4/saved_acl2")
 '(inhibit-startup-screen t)
 '(ispell-program-name "/usr/bin/aspell")
 '(js-indent-level 2)
 '(kept-new-versions 6)
 '(kept-old-versions 2)
 '(lisp-body-indent 2)
 '(lisp-indent-maximum-backtracking 4)
 '(lisp-tag-body-indentation 2)
 '(lisp-tag-indentation 2)
 '(mail-user-agent 'mu4e-user-agent)
 '(message-kill-buffer-on-exit t)
 '(message-send-mail-function 'smtpmail-send-it)
 '(mu4e-compose-context-policy nil)
 '(mu4e-context-policy 'pick-first)
 '(mu4e-drafts-folder "/drafts")
 '(mu4e-get-mail-command "offlineimap")
 '(mu4e-maildir "/home/alex/mail")
 '(mu4e-maildir-shortcuts
   '(("/personal/INBOX" . 105)
     ("/personal/[Gmail].Sent Mail" . 115)
     ("/personal/[Gmail].Trash" . 116)
     ("/personal/[Gmail].All Mail" . 97)))
 '(mu4e-sent-folder "/sent")
 '(mu4e-sent-messages-behavior 'delete)
 '(mu4e-trash-folder "/trash")
 '(multi-term-program "/bin/ksh")
 '(org-agenda-files '("~/doc/todo.org"))
 '(org-default-notes-file "~/doc/notes.org")
 '(org-journal-dir "~/doc/journal/")
 '(org-return-follows-link t)
 '(package-selected-packages
   '(sqlite3 websocket geiser-guile geiser-mit geiser yasnippet proof-general yul-mode rust-mode go-mode forge sml-basis sml-mode virtualenv helm-lsp lsp-mode json-mode lua-mode yaml-mode mu4e pianobar direnv use-package user-package bind-key markdown-mode typescript-mode company-solidity solidity-flycheck solidity-mode slime company magit d pinentry helm-company helm-projectile helm gerbil-mode gerbil auth-sources-pass pass auth-source-pass org-journal slime-company pdf-tools paredit org multiple-cursors flycheck exec-path-from-shell diminish))
 '(projectile-mode-line
   '(:eval
     (if
         (file-remote-p default-directory)
         " P"
       (format " P %s"
               (projectile-project-name)))))
 '(safe-local-variable-values
   '((eval let
           ((root-dir-unexpanded
             (locate-dominating-file default-directory ".dir-locals.el")))
           (when root-dir-unexpanded
             (let*
                 ((root-dir
                   (file-local-name
                    (expand-file-name root-dir-unexpanded)))
                  (root-dir*
                   (directory-file-name root-dir)))
               (unless
                   (boundp 'geiser-guile-load-path)
                 (defvar geiser-guile-load-path 'nil))
               (make-local-variable 'geiser-guile-load-path)
               (require 'cl-lib)
               (cl-pushnew root-dir* geiser-guile-load-path :test #'string-equal))))
     (eval progn
           (require 'lisp-mode)
           (defun emacs27-lisp-fill-paragraph
               (&optional justify)
             (interactive "P")
             (or
              (fill-comment-paragraph justify)
              (let
                  ((paragraph-start
                    (concat paragraph-start "\\|\\s-*\\([(;\"]\\|\\s-:\\|`(\\|#'(\\)"))
                   (paragraph-separate
                    (concat paragraph-separate "\\|\\s-*\".*[,\\.]$"))
                   (fill-column
                    (if
                        (and
                         (integerp emacs-lisp-docstring-fill-column)
                         (derived-mode-p 'emacs-lisp-mode))
                        emacs-lisp-docstring-fill-column fill-column)))
                (fill-paragraph justify))
              t))
           (setq-local fill-paragraph-function #'emacs27-lisp-fill-paragraph))
     (eval modify-syntax-entry 43 "'")
     (eval modify-syntax-entry 36 "'")
     (eval modify-syntax-entry 126 "'")
     (eval let
           ((root-dir-unexpanded
             (locate-dominating-file default-directory ".dir-locals.el")))
           (when root-dir-unexpanded
             (let*
                 ((root-dir
                   (expand-file-name root-dir-unexpanded))
                  (root-dir*
                   (directory-file-name root-dir)))
               (unless
                   (boundp 'geiser-guile-load-path)
                 (defvar geiser-guile-load-path 'nil))
               (make-local-variable 'geiser-guile-load-path)
               (require 'cl-lib)
               (cl-pushnew root-dir* geiser-guile-load-path :test #'string-equal))))
     (eval with-eval-after-load 'yasnippet
           (let
               ((guix-yasnippets
                 (expand-file-name "etc/snippets/yas"
                                   (locate-dominating-file default-directory ".dir-locals.el"))))
             (unless
                 (member guix-yasnippets yas-snippet-dirs)
               (add-to-list 'yas-snippet-dirs guix-yasnippets)
               (yas-reload-all))))
     (eval setq-local guix-directory
           (locate-dominating-file default-directory ".dir-locals.el"))
     (eval add-to-list 'completion-ignored-extensions ".go")
     (eval cl-flet
           ((enhance-imenu-lisp
             (&rest keywords)
             (dolist
                 (keyword keywords)
               (let
                   ((prefix
                     (when
                         (listp keyword)
                       (cl-second keyword)))
                    (keyword
                     (if
                         (listp keyword)
                         (cl-first keyword)
                       keyword)))
                 (add-to-list 'lisp-imenu-generic-expression
                              (list
                               (purecopy
                                (concat
                                 (capitalize keyword)
                                 (if
                                     (string=
                                      (substring-no-properties keyword -1)
                                      "s")
                                     "es" "s")))
                               (purecopy
                                (concat "^\\s-*("
                                        (regexp-opt
                                         (list
                                          (if prefix
                                              (concat prefix "-" keyword)
                                            keyword)
                                          (concat prefix "-" keyword))
                                         t)
                                        "\\s-+\\(" lisp-mode-symbol-regexp "\\)"))
                               2))))))
           (enhance-imenu-lisp
            '("bookmarklet-command" "define")
            '("class" "define")
            '("command" "define")
            '("ffi-method" "define")
            '("ffi-generic" "define")
            '("function" "define")
            '("internal-page-command" "define")
            '("internal-page-command-global" "define")
            '("mode" "define")
            '("parenscript" "define")
            "defpsmacro"))
     (c-file-offsets
      (innamespace . 0)
      (substatement-open . 0)
      (c . c-lineup-dont-change)
      (inextern-lang . 0)
      (comment-intro . c-lineup-dont-change)
      (arglist-cont-nonempty . c-lineup-arglist)
      (block-close . 0)
      (statement-case-intro . ++)
      (brace-list-intro . ++)
      (cpp-define-intro . +))
     (c-auto-align-backslashes)
     (whitespace-style quote
                       (face trailing empty tabs))
     (whitespace-action)
     (Package . XLIB)
     (Syntax . Common-lisp)
     (Package . ALPHA-AXP-INTERNALS)
     (Package . COMPILER)
     (Base . 8)
     (Package . system)
     (Syntax . Zetalisp)
     (Package . COMMON-LISP-USER)
     (Lowercase . T)
     (Package . AMD64-INTERNALS)
     (Package . POWERPC-INTERNALS)
     (Syntax . Common-Lisp)
     (Syntax . Ansi-common-lisp)
     (Base . 10)
     (Package SCREAMER :USE CL :COLON-MODE :EXTERNAL)
     (Lowercase . Yes)
     (Tab-Width . 4)))
 '(send-mail-function 'mailclient-send-it)
 '(slime-default-lisp 'sbcl t)
 '(sml/active-background-color "#98ece8")
 '(sml/active-foreground-color "#424242")
 '(sml/inactive-background-color "#4fa8a8")
 '(sml/inactive-foreground-color "#424242")
 '(smtpmail-debug-info t)
 '(smtpmail-default-smtp-server "smtp.gmail.com")
 '(smtpmail-smtp-server "smtp.gmail.com")
 '(smtpmail-smtp-service 587)
 '(smtpmail-smtp-user "e.alexander.segura")
 '(smtpmail-stream-type 'starttls)
 '(starttls-use-gnutls t)
 '(tab-always-indent 'complete)
 '(tetris-update-speed-function 'my-tetris-update-speed-function)
 '(tramp-default-method "ssh")
 '(truncate-lines nil)
 '(typescript-indent-level 4)
 '(user-full-name "Alex Segura")
 '(user-mail-address "e.alexander.segura@gmail.com")
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   '((20 . "#f36c60")
     (40 . "#ff9800")
     (60 . "#fff59d")
     (80 . "#8bc34a")
     (100 . "#81d4fa")
     (120 . "#4dd0e1")
     (140 . "#b39ddb")
     (160 . "#f36c60")
     (180 . "#ff9800")
     (200 . "#fff59d")
     (220 . "#8bc34a")
     (240 . "#81d4fa")
     (260 . "#4dd0e1")
     (280 . "#b39ddb")
     (300 . "#f36c60")
     (320 . "#ff9800")
     (340 . "#fff59d")
     (360 . "#8bc34a")))
 '(vc-annotate-very-old-color nil)
 '(vc-follow-symlinks t)
 '(version-control t)
 '(virtualenv-root "/home/alex/src/arbitrary/audit/audit-template/.venv/"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(helm-source-header ((t (:extend t :background "#22083397778B" :foreground "white" :weight bold :family "Sans Serif")))))
