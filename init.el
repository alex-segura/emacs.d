;;; package --- Summary
;;; This is my init.el file, there are many like it, but this one is mine.
;;; Commentary:
;;; Code:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main

(setq custom-file (expand-file-name "~/.emacs.d/custom.el"))

(when (file-exists-p custom-file)
  (load-file custom-file))

(defvar user-source-dir
  (expand-file-name
   (cond ((eql system-type 'darwin) "~/Code/")
         ((eql system-type 'gnu/linux) "~/src/")
         ((eql system-type 'berkeley-unix) "~/src/")))
  "Root directory of user source code.")

(when (eql system-type 'darwin)
  (setq
   mac-option-modifier  'super
   mac-command-modifier 'meta
   default-directory    (expand-file-name "~/"))
  (setq-default
   mac-option-key-is-meta  nil
   mac-command-key-is-meta t))

(defun create-scratch-buffer ()
  "Create a new *scratch* buffer, set to the INITIAL-MAJOR-MODE."
  (interactive)
  (let ((buf (generate-new-buffer "*scratch*")))
    (switch-to-buffer buf)
    (insert initial-scratch-message)
    (funcall initial-major-mode)))

(eval-and-compile
  (defmacro define-edit-command (name name-and-location &rest options)
    "Define an edit command NAME.
NAME-AND-LOCATION should be a pair of file name and file location.  The alist
OPTIONS are additional information to be included in the command definition.
Supported options are :key, the key sequence to bind to the command, and
:documentation, a docstring to be added to the function."
    (let* ((fn-name (make-symbol (format "edit-%s" name)))
           (file-name (car name-and-location))
           (location (cadr name-and-location))
           (doc (cadr (assoc :documentation options)))
           (key (cadr (assoc :key options)))
           (body `((interactive)
                   (if (member ,file-name (mapcar 'buffer-name (buffer-list)))
                       (switch-to-buffer ,file-name)
                     (find-file (expand-file-name ,location))))))
      `(progn
         (defun ,fn-name ()
           ,@(if doc (cons doc body) body))
         ,(when key `(global-set-key (kbd ,key) ',fn-name))))))

(define-edit-command init-file
  ("init.el" "~/.emacs.d/init.el")
  (:key "C-c e i")
  (:documentation "Edit .emacs initialization file."))

(scroll-bar-mode -1)
(menu-bar-mode -1)
(tool-bar-mode -1)
(show-paren-mode +1)
(column-number-mode +1)
(global-eldoc-mode +1)
(display-time-mode 1)

(fset 'yes-or-no-p 'y-or-n-p)

(eval-after-load "eshell"
  '(defun eshell/clear ()
     "Like the shell command, erases the text above the current prompt"
     (let ((inhibit-read-only t))
       (eshell-send-input)
       (erase-buffer))))

(setenv "INSIDE_EMACS" (format "%s,comint" emacs-version))

(defun camel->kebab ()
  (interactive)
  (replace-regexp "\\([A-Z]\\)" "-\\1" nil (region-beginning) (region-end))
  (downcase-region (region-beginning) (region-end)))

(defun my-tetris-update-speed-function (shapes rows) 0.3)
(custom-set-variables
 '(tetris-update-speed-function 'my-tetris-update-speed-function))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Packages

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'bind-key)

(use-package bind-key
  :ensure t)

(use-package pass
  :if (executable-find "pass")
  :ensure t
  :config
  (require 'auth-source-pass)
  (auth-source-pass-enable))

(use-package exec-path-from-shell
  :if (eql window-system 'x)
  :ensure t
  :config
  (exec-path-from-shell-initialize))

(use-package direnv
  :if (executable-find "direnv")
  :ensure t
  :config
  (require 'direnv)
  (direnv-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Appearance

(setq frame-background-mode nil)
(set-frame-font "Terminus-10")
(invert-face 'default)
(let ((default-background (face-attribute 'default :background))
      (mode-line-background (face-attribute 'mode-line :background)))
  (dolist (face-settings
           `((mode-line          :box nil)
             (mode-line-inactive :box nil)
             (fringe             :background ,default-background)
             (vertical-border    :foreground ,mode-line-background)))
    (cl-destructuring-bind (face attribute value) face-settings
      (set-face-attribute face nil attribute value))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Editing

(delete-selection-mode 1)
(customize-set-variable 'tab-always-indent 'complete)
(setq-default indent-tabs-mode nil)
(add-hook 'before-save-hook 'whitespace-cleanup)
(add-hook 'gud-mode-hook 'gdb-many-windows)

(global-unset-key (kbd "<insertchar>"))
(global-unset-key (kbd "<insert>"))

(use-package diminish
  :ensure t
  :config
  (diminish 'eldoc-mode)
  (eval-after-load 'autorevert
    '(diminish 'auto-revert-mode))
  (eval-after-load 'abbrev-mode
    '(diminish 'abbrev-mode))
  (eval-after-load 'auto-fill-mode
    '(diminish 'auto-fill-mode)))

(use-package company
  :init (require 'color)
  :ensure t
  :hook (after-init-hook . global-company-mode)
  :diminish company-mode
  :custom
  (company-tooltip-limit 20)
  (company-idle-delay 0.3)
  (company-echo-delay 0)
  (company-begin-commands '(self-insert-command)))

(use-package multiple-cursors
  :ensure t
  :diminish multiple-cursors-mode
  :bind (("C-S-c C-S-c" . mc/edit-lines)
         ("C->" . mc/mark-next-like-this)
         ("C-<" . mc/mark-previous-like-this)
         ("C-c C-<" . mc/mark-all-like-this)))

(require 'dired-x)
(diminish 'dired-omit-mode)
(add-hook 'dired-mode-hook 'dired-omit-mode)

(use-package paredit
  :ensure t
  :diminish paredit-mode
  :bind (:map paredit-mode-map
              ("C-<left>" . paredit-backward-slurp-sexp)
              ("C-<right>" . paredit-forward-slurp-sexp)
              ("{" . paredit-open-curly)
              ("}" . paredit-close-curly))
  :hook ((scheme-mode
          emacs-lisp-mode
          lisp-mode
          ielm-mode
          slime-mode
          slime-repl-mode
          inferior-scheme-mode)
         . #'enable-paredit-mode)
  :config
  (dolist (k '("RET" "C-m" "C-j"))
    (unbind-key k paredit-mode-map))
  (eldoc-add-command 'paredit-backward-delete 'paredit-close-round)
  (add-to-list 'paredit-space-for-delimiter-predicates
               (lambda (endp delimiter)
                 (declare (ignore endp delimiter)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Helm

(use-package helm
  :ensure t
  :diminish helm-mode
  :init
  (progn
    (require 'helm-config)
    (helm-mode 1))
  :bind (("M-x" . helm-M-x)
         ("M-y" . helm-show-kill-ring)
         ("C-x C-f" . helm-find-files)
         ("C-x C-b" . helm-buffers-list)
         ("C-x b" . helm-mini)
         :map helm-map
         ("<tab>" . helm-execute-persistent-action)
         ("C-i" . helm-execute-persistent-action)
         ("C-z" . helm-select-action))
  :config
  (helm-mode 1)
  (global-set-key (kbd "C-c h") 'helm-command-prefix)
  (global-unset-key (kbd "C-x c"))
  (setq helm-autoresize-max-height 0
        helm-autoresize-min-height 30)
  (helm-autoresize-mode 1)
  (diminish 'helm-mode))

(use-package helm-company
  :ensure t
  :after company
  :bind (:map company-mode-map ("C-:" . helm-company)
         :map company-active-map ("C-:" . helm-company)))

(use-package helm-projectile
  :ensure t
  :after helm
  :bind (:map helm-command-map
         ("C-p h" . helm-projectile)
         ("C-p f" . helm-projectile-find-file)
         ("C-p a" . helm-projectile-ag)))

(use-package helm-pass
  :if (executable-find "pass")
  :ensure t)

(use-package helm-ag
  :if (executable-find "ag")
  :ensure t
  :bind (:map helm-command-map
              ("C-p h" . helm-projectile)
              ("f" . helm-projectile-find-file)
              ("g" . helm-projectile-grep)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Coding

(use-package flycheck
  :ensure t
  :diminish flycheck-mode
  :hook (after-init-hook . global-flycheck-mode)
  :bind (("M-n" . flycheck-next-error)
         ("M-p" . flycheck-previous-error)))

(use-package magit
  :if (executable-find "git")
  :ensure t
  :bind (("C-x g" . magit-status)))

(use-package forge
  :ensure t
  :after magit)

(use-package sqlite3
  :ensure t)

(bind-key (kbd "<f5>") 'compile)

(use-package yasnippet
  :ensure t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; C

(use-package helm-cscope
  :if (executable-find "cscope")
  :bind (:map helm-command-map
              ("y" . helm-cscope-find-this-symbol)
              ("g" . helm-cscope-find-global-definition)
              ("z" . helm-cscope-find-assignments-to-this-symbol)))

(setq c-default-style "k&r")
(setq c-basic-offset 4)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Common Lisp

(use-package slime
  :defines my-hyperspec-root
  :ensure t
  :bind (:map slime-mode-map ("C-c M-e" . macrostep-expand))
  :custom
  (slime-net-coding-system 'utf-8-unix)
  :config
  (defvar my-hyperspec-root
    (cond ((eql system-type 'berkeley-unix)
           "/usr/local/share/doc/clisp-hyperspec/")
          ((eql system-type 'darwin)
           (expand-file-name "~/Documents/HyperSpec/"))
          ((eql system-type 'gnu/linux)
           (let* ((lispworks-home "/usr/local/lib/LispWorksPersonal/lib/6-1-0-0/")
                  (linux-1-hyperspec "/usr/share/doc/hyperspec/HyperSpec/")
                  (linux-2-hyperspec
                   (expand-file-name "manual/online/CLHS/" lispworks-home))
                  (linux-3-hyperspec "/usr/share/doc/clhs/HyperSpec/"))
             (cond ((file-exists-p linux-1-hyperspec) linux-1-hyperspec)
                   ((file-exists-p linux-2-hyperspec) linux-2-hyperspec)
                   ((file-exists-p linux-3-hyperspec) linux-3-hyperspec))))))
  (when (and my-hyperspec-root (file-exists-p my-hyperspec-root))
    (let ((hyperspec-symtab
           (expand-file-name "Data/Map_Sym.txt" my-hyperspec-root)))
      (setq common-lisp-hyperspec-root (concat "file://" my-hyperspec-root)
            common-lisp-hyperspec-symbol-table hyperspec-symtab)))
  (setq slime-lisp-implementations
        '((sbcl ("sbcl" "-quiet") :coding-system utf-8-unix)))
  (require 'slime-autoloads)
  (slime-setup '(slime-fancy
                 slime-xref-browser
                 slime-indentation
                 slime-presentation-streams
                 slime-tramp))
  (load "~/.quicklisp/log4slime-setup.el")
  (global-log4slime-mode 1))

(dolist (binding '((dynamic-let . let)
                   (defglobal . defvar)
                   (case-using . case)
                   (event-case . case)))
  (put (car binding) 'common-lisp-indent-function
       (get (cdr binding) 'common-lisp-indent-function)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Device Tree

(use-package dts-mode
  :ensure t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Email

(defvar mu4e-site-lisp
  (cond ((eql system-type 'gnu/linux)
         "/usr/share/emacs/site-lisp/mu4e/")
        ((eql system-type 'berkeley-unix)
         "/usr/local/share/emacs/site-lisp/mu4e/")
        ((eql system-type 'darwin)
         (let* ((out (shell-command-to-string "readlink `which mu`"))
                (bin (string-trim-right out))
                (dir (shell-command-to-string (concat "dirname " bin))))
           (expand-file-name "../share/emacs/site-lisp/mu4e/" dir)))))

(use-package pinentry
  :ensure t
  :config
  (pinentry-start))

(use-package mu4e
  :load-path mu4e-site-lisp
  :defines mu4e-account-alist mu4e-default-account
  :functions mu4e-set-account mu4e-find-account mu4e-set-account-vars
  :hook (mu4e-compose-pre . mu4e-set-account)
  :config
  (defvar mu4e-account-alist
    '(("gmail"
       (mu4e-sent-folder "/gmail/[Gmail].Sent Mail")
       (mu4e-drafts-folder "/gmail/[Gmail].Drafts")
       (mu4e-trash-folder "/gmail/[Gmail].Bin")
       (user-mail-address "e.alexander.segura@gmail.com")
       (smtpmail-default-smtp-server "smtp.gmail.com")
       (smtpmail-smtp-user "e.alexander.segura")
       (smtpmail-smtp-server "smtp.gmail.com")
       (smtpmail-stream-type starttls)
       (smtpmail-smtp-service 587))
      ("dev"
       (mu4e-sent-folder "/dev/Sent")
       (mu4e-drafts-folder "/dev/Drafts")
       (mu4e-trash-folder "/dev/Trash")
       (user-mail-address "alex@lispm.dev")
       (smtpmail-default-smtp-server "smtp.fastmail.com")
       (smtpmail-smtp-user "alex@lispm.dev")
       (smtpmail-smtp-server "smtp.fastmail.com")
       (smtpmail-stream-type starttls)
       (smtpmail-smtp-service 587))))
  (require 'mu4e)
  (defvar mu4e-default-account "gmail")
  (defun mu4e-set-account-vars (account-vars)
    "Set ACCOUNT-VARS, an alist of var, value pairs."
    (mapc (lambda (var) (set (car var) (cadr var))) account-vars))
  (defun mu4e-find-account (account)
    "Find the mu4e ACCOUNT alist."
    (cdr (assoc account mu4e-account-alist)))
  (defun mu4e-set-account ()
    "Set the account for composing a message."
    (interactive)
    (let* ((account
            (if mu4e-compose-parent-message
                (let ((maildir (mu4e-message-field mu4e-compose-parent-message :maildir)))
                  (string-match "/\\(.*?\\)/" maildir)
                  (match-string 1 maildir))
              (completing-read (format "Compose with account: (%s) "
                                       (mapconcat #'(lambda (var) (car var))
                                                  mu4e-account-alist "/"))
                               (mapcar #'(lambda (var) (car var)) mu4e-account-alist)
                               nil t nil nil (caar mu4e-account-alist))))
           (account-vars (mu4e-find-account account)))
      (mu4e-set-account-vars account-vars)))
  (mu4e-set-account-vars (mu4e-find-account mu4e-default-account)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Hoon

(defvar hoon-mode-source-dir
  (expand-file-name "github/urbit/urbit/extras/hoon-mode.el/" user-source-dir))

(use-package hoon-mode
  :if (file-exists-p hoon-mode-source-dir)
  :load-path hoon-mode-source-dir
  :bind (:map hoon-mode-map
              ("C-c r" . hoon-eval-region-in-herb)
              ("C-c b" . hoon-eval-buffer-in-herb))
  :mode (("\\.hoon$" . hoon-mode))
  :config (require 'hoon-mode))

(defvar urbit-mode-source-dir
  (expand-file-name "github/urbit/urbit-api.el/" user-source-dir))

(use-package urbit-mode
  :if (file-exists-p urbit-mode-source-dir)
  :load-path urbit-mode-source-dir
  :custom
  (urbit-ship-host "dasrun-fadben.arvo.network")
  :config
  (require 'urbit))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Lua

(use-package lua-mode :ensure t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Maxima

(defvar maxima-site-lisp
  (cond ((eql system-type 'darwin)
         (let* ((out (shell-command-to-string "readlink `which maxima`"))
                (bin (string-trim-right out))
                (dir (shell-command-to-string (concat "dirname " bin))))
           (expand-file-name "../share/emacs/site-lisp/" dir)))
        ((eql system-type 'gnu/linux)
         "/usr/share/emacs/site-lisp/maxima/")))

(use-package maxima
  :if (executable-find "maxima")
  :load-path maxima-site-lisp
  :config
  (autoload 'imaxima "imaxima" "Frontend for maxima with Image support" t)
  (autoload 'imath-mode "imath" "Imath mode for math formula input" t)
  (setq imaxima-use-maxima-mode-flag t)
  (setq imaxima-fnt-size "large")
  (setq imaxima-pt-size 12))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Music

(use-package pianobar
  :if (executable-find "pianobar")
  :ensure t
  :bind (:map pianobar-mode-map
              ("C-c -" . pianobar-volume-down)
              ("C-c +" . pianobar-volume-up)
              ("C-c p" . pianobar-play-or-pause)
              ("C-c n" . pianobar-next-song))
  :config
  (setq pianobar-config t
        pianobar-enable-modeline nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; OCaml

(use-package tuareg
  :ensure t
  :mode (("\\.ml[ily]?$" . tuareg-mode)
         ("\\.topml$" . tuareg-mode)))

(use-package merlin
  :ensure t
  :hook ((tuareg-mode . merlin-mode)
         (merlin-mode . company-mode))
  :config
  (let ((opam-share (ignore-errors (car (process-lines "opam" "config" "var" "share")))))
    (when (and opam-share (file-directory-p opam-share))
      (add-to-list 'load-path (expand-file-name "emacs/site-lisp" opam-share))))
  (setq merlin-command 'opam
        merlin-error-after-save nil))

(use-package utop
  :ensure t
  :hook ((tuareg-mode . utop-minor-mode))
  :config
  (autoload 'utop-minor-mode "utop" "Minor mode for utop" t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Org

(use-package org
  :ensure t
  :defines todo-list-file
  :functions edit-todo-list
  :bind (("C-c a" . org-agenda)
         ("C-c c" . org-capture))
  :hook ((org-mode . org-indent-mode)
         (org-mode . auto-fill-mode))
  :config
  (setq-default org-hide-leading-stars t
                org-odd-levels-only t)
  (defvar todo-list-file "~/doc/todo.org")
  (define-edit-command todo-list
    ("todo.org" todo-list-file)
    (:key "C-c e t")
    (:documentation "Edit my main TODO list."))
  (setq org-latex-compiler "xelatex")
  (setq org-latex-pdf-process '("latexmk -xelatex -quiet -shell-escape -f %f"))
  (setq-default TeX-engine 'xetex)
  (setq-default TeX-PDF-mode t))

(use-package org-journal
  :ensure t
  :after org
  :defines org-journal-header
  :functions org-journal-insert-header
  :config
  (require 'epa-file)
  (defvar org-journal-header
    (format "# -*- mode: org; epa-file-encrypt-to: (\"%s\") -*-\n\n"
            user-mail-address))
  (defun org-journal-insert-header (fn &rest args)
    (let* ((entry-path
            (expand-file-name (format-time-string org-journal-file-format)
                              org-journal-dir)))
      (apply fn args)
      (unless (file-exists-p entry-path)
        (find-file entry-path)
        (save-excursion
          (goto-char (point-min))
          (insert org-journal-header)))))
  (advice-add 'org-journal-new-entry :around #'org-journal-insert-header))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Prolog

(use-package prolog-mode
  :mode (("\\.pl$" . prolog-mode))
  :custom
  (prolog-system 'swi)
  (prolog-program-switches '((swi ("-O"))
                             (t nil)))
  (prolog-electric-if-then-else-flag t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Python

(use-package virtualenv :ensure t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Scheme

(use-package geiser
  :ensure t
  :hook (geiser-repl-mode . enable-paredit-mode))

(use-package geiser-mit
  :ensure t
  :mode (("\\.sls\\'" . scheme-mode)))

(use-package geiser-guile
  :ensure t
  :custom
  (geiser-default-implementation 'guile))

(with-eval-after-load 'geiser-guile
  (let ((guix-source-directory (expand-file-name "guix/" user-source-dir)))
    (when (file-exists-p guix-source-directory)
      (add-to-list 'geiser-guile-load-path guix-source-directory))))

(with-eval-after-load 'yasnippet
  (let ((guix-snippet-directory (expand-file-name "guix/etc/snippets/yas" user-source-dir)))
    (when (file-exists-p guix-snippet-directory)
      (add-to-list 'yas-snippet-dirs guix-snippet-directory))))

(use-package macrostep-geiser
  :after geiser-mode
  :hook (geiser-mode . macrostep-geiser-setup))

(use-package macrostep-geiser
  :after geiser-repl
  :hook (geiser-repl-mode . macrostep-geiser-setup))

;; (use-package racket-mode
;;   :ensure t
;;   :hook (racket-mode . enable-paredit-mode))

(defvar scheme48-directory
  (cond ((eql system-type 'gnu/linux)
         (expand-file-name "aur/scheme48/src/scheme48-1.9.2/emacs/"
                           user-source-dir))))

(when (and scheme48-directory (file-exists-p scheme48-directory))
  (add-to-list 'load-path scheme48-directory)
  (require 'cmuscheme48))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Standard ML

(use-package sml-mode
  :ensure t
  :mode (("\\.sml\\'" . sml-mode)
         ("\\.sig\\'" . sml-mode)
         ("\\.lex\\'" . sml-lex-mode))
  :custom
  (sml-program-name "smlnj")
  (sml-indent-level 2)
  :config
  (autoload 'sml-mode "sml-mode" "Major mode for editing SML." t)
  (autoload 'run-sml "sml-proc" "Run an inferior SML process." t))

(use-package sml-basis :ensure t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ACL2

(defvar acl2-emacs-directory
  (cond ((eql system-type 'gnu/linux) "/opt/acl2/emacs/")))

(defun load-acl2-mode ()
  "Load ACL2 elisp files."
  (interactive)
  (when (and acl2-emacs-directory (file-exists-p acl2-emacs-directory))
    (add-to-list 'load-path acl2-emacs-directory)
    (load "emacs-acl2.el")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PDF Tools

(use-package pdf-tools
  :ensure t
  :magic ("%PDF" . pdf-view-mode)
  :config
  (pdf-tools-install :no-query))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ASM

(use-package asm-mode
  :hook (asm-mode . my-asm-mode-hook)
  :functions my-asm-mode-hook asm-calculate-indentation asm-period
  :bind (:map asm-mode-map
              ("<ret>" . newline)
              ("C-j" . newline)
              ("C-m" . newline))
  :config
  (defun my-asm-mode-hook ()
    (local-unset-key (vector asm-comment-char))
    (setq indent-tabs-mode nil))
  (defun asm-calculate-indentation ()
    (or
     ;; Flush labels to the left margin.
     (and (looking-at "\\(\\sw\\|\\s_\\)+:") 0)
     ;; Same thing for `;;;' comments.
     (and (looking-at "\\s<\\s<\\s<") 0)
     ;; And same for `.' directives.
     (and (looking-at "\\.\\(\\s\\)+") 0)
     ;; Simple `;' comments go to the comment-column.
     (and (looking-at "\\s<\\(\\S<\\|\\'\\)") comment-column)
     ;; The rest goes at the first tab stop.
     (or (indent-next-tab-stop 0))))
  (defun asm-period ()
    (interactive)
    (call-interactively 'self-insert-command)
    (save-excursion
      (tab-to-tab-stop))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ethereum/Blockchain

(use-package json-mode :ensure t)
(use-package yaml-mode :ensure t)
(use-package markdown-mode :ensure t)

(use-package solidity-mode
  :ensure t
  :bind (:map solidity-mode-map
              ("C-c C-g" . solidity-estimate-gas-at-point))
  :hook ((solidity-mode . (lambda ()
                            (setq indent-tabs-mode nil)))))

(use-package yul-mode :ensure t)

(use-package typescript-mode
  :ensure t
  :mode (("\\.ts\\'" . typescript-mode)
         ("\\.tsx\\'" . typescript-mode)))

(use-package go-mode :ensure t)
(use-package rust-mode :ensure t)

(provide 'init)
;;; init.el ends here
